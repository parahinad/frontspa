import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";

i18n.use(LanguageDetector).init({
  // we init with resources
  resources: {
    EN: {
      translations: {
        Login: "Login",
        Signup: "Sign up",
        Comp: "Customer",
        CComp: "Provider",
        Name: "Name",
        Email: "Email",
        Password: "Password",
        Profile: "Profile",
        Date: "Date",
        Signout: "Sign out",
        EditP: "Edit profile",
        Edit: "Edit",
        Delete: "Delete",
        Failiture: "Failiture",
        Loading: "Loading",
        Statistics: "Statisctics",
        Backup: "Databasse backup",
        Admin: "Admin",
        Save: "Save",
        Phone: "Phone number",
        Address: "Address",
        City: " ",
        Street: "St ",
        House: " ",
        FCountry: "Country",
        FCity: "City",
        FStreet: "Street",
        FHouse: "House number",
        Services: "Procedures",
        AddS: "Add procedure",
        Desc: "Description",
        Price: "Price",
        minA: "Minimum area",
        maxA: "Maximun area",
        rType: "Room type",
        pMes: "₴",
        Add: "Add",
        Min: "Minimum age",
        Max: "Maximum age",
        Pick: "Pick",
        SelectDate: "Select date and time of procedure",

        pickSId: "Pick service id:",
        pickRId: "Pick room id:",
        "Select an id": "Select an id",
        Dur: "Duration",
        Price: "Price",
        DurA: "min",
        View: "View procedures",
        PName: "Name",

        DName: "Company name",
        DFName: "First name",
        DLName: "Last name",
        Age: "Age",
        Gender: "Gender",
        checkCred: "Check the correctness of the entered data!",
        EFName: "Your first name format is wrong! Please try again.",
        ELName: "Your last name format is wrong! Please try again.",
        EAge: "Your age format is wrong! Please try again.",
        EGender: "Your gender format is wrong! Please try again.",
        EEmail: "Your email format is wrong! Please try again.",
        EPass: "Your password format is wrong! Please try again.",
        EPhone: "Your phone number format is wrong! Please try again.",
        ECountry: "Your country format is wrong! Please try again.",
        ECity: "Your city format is wrong! Please try again.",
        EStreet: "Your street format is wrong! Please try again.",
        EHouse: "Your house format is wrong! Please try again.",
        EType: "Your duration format is wrong! Please try again.",
        EFloor: "Your floor format is wrong! Please try again.",
        EWCount: "Your windows number format is wrong! Please try again.",
        EArea: "Your area format is wrong! Please try again.",
        EDesc: "Your description format is wrong! Please try again.",
        EMinA: "Your minimum age format is wrong! Please try again.",
        EMaxA: "Your maximum age format is wrong! Please try again.",
        EEroor: "Something went wrong! Please? try again.",
        Eprice: "Your price format is wrong! Please try again.",
        Temp: "Temperature",
        Hum: "Humidity",
        More: "More info",

        Search: "Search",
        eExist: "Such email already exist in system!",
      },
    },
    UA: {
      translations: {
        Login: "Авторизуватися",
        Comp: "Користувач",
        CComp: "Провайдер",
        Name: "Ім'я",
        Email: "Електронна пошта",
        Password: "Пароль",
        Profile: "Особистий профіль",
        Date: "Дата",
        Signup: "Зареєструватися",
        Signout: "Вийти",
        EditP: "Редагувати профіль",
        Edit: "Редагувати",
        Delete: "Видалити",
        Failiture: "Помилка",
        Loading: "Загрузка",
        Save: "Зберегти",
        Backup: "Резервне копіювання даних",
        Admin: "Адміністратор",
        Phone: "Номер телефону",
        Address: "Адреса",
        City: "м.",
        Street: "вул.",
        House: "буд.",
        FCountry: "Країна",
        FCity: "Місто",
        FStreet: "Вулиця",
        FHouse: "Номер будинку",
        Services: "Процедури",
        AddS: "Додати процедуру",
        pMes: "₴",
        Add: "Додати",
        Price: "Ціна",
        Dur: "Тривалість",
        DurA: "хв",
        View: "Переглянути процедури",
        Min: "Мінімальний вік",
        Max: "Максимальний вік",
        Pick: "Обрати",
        Desc: "Опис",
        SelectDate: "Оберіть дату та час процедури",
        Temp: "Температура",
        Hum: "Вологість",
        More: "Більше інформації",

        PName: "Назва",
        DName: "Назва компанії",
        DFName: "Ім'я",
        DLName: "Прізвище",
        Age: "Вік",
        Gender: "Пол",
        checkCred: "Перевірте правильність введених даних!",
        EFName:
          "Формат вашого імені неправильний! Будь ласка, спробуйте ще раз.",
        ELName:
          "Формат вашого прізвища неправильний! Будь ласка, спробуйте ще раз.",
        EAge: "Формат вашого віку неправильний! Будь ласка, спробуйте ще раз.",
        EGender:
          "Формат вашого полу неправильний! Будь ласка, спробуйте ще раз.",
        EName: "Формат вашої назви неправильний! Будь ласка, спробуйте ще раз.",
        EEmail:
          "Формат електронної пошти неправильний! Будь ласка, спробуйте ще раз.",
        EPass:
          "Формат вашого пароля неправильний! Будь ласка, спробуйте ще раз.",
        EPhone:
          "Формат вашого мобільного номера неправильний! Будь ласка, спробуйте ще раз.",
        ECountry:
          "Формат вашої країни неправильний! Будь ласка, спробуйте ще раз.",
        ECity:
          "Формат вашого міста неправильний! Будь ласка, спробуйте ще раз.",
        EStreet:
          "Формат вашої вулиці неправильний! Будь ласка, спробуйте ще раз.",
        EHouse:
          "Формат вашого номеру будинку неправильний! Будь ласка, спробуйте ще раз.",
        EType: "Формат тривалості неправильний! Будь ласка, спробуйте ще раз.",
        EFloor:
          "Формат номеру поверху неправильний! Будь ласка, спробуйте ще раз.",
        EWCount:
          "Формат кількості вікон неправильний! Будь ласка, спробуйте ще раз.",
        EArea:
          "Формат площі кімнати неправильний! Будь ласка, спробуйте ще раз.",
        EDesc: "Формат опису неправильний! Будь ласка, спробуйте ще раз.",
        EEroor: "Возникла помилка. Будь ласка, спробуйте ще раз.",
        EMinA:
          "Формат мінімального віку неправильний! Будь ласка, спробуйте ще раз.",
        EMaxA:
          "Формат максимального віку неправильний! Будь ласка, спробуйте ще раз.",
        Eprice: "Формат ціни неправильний! Будь ласка, спробуйте ще раз.",

        Search: "Пошук процедур",
        eExist: "Така електронна пошта вже існує в системі!",
      },
    },
  },
  fallbackLng: "en",
  debug: true,

  // have a common namespace used around the full app
  ns: ["translations"],
  defaultNS: "translations",

  keySeparator: false, // we use content as keys

  interpolation: {
    escapeValue: false, // not needed for react!!
    formatSeparator: ",",
  },

  react: {
    wait: true,
  },
});

export default i18n;
