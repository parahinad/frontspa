import React from "react";
import "./App.css";
import { observer } from "mobx-react";
import { Route, Switch, BrowserRouter, Redirect } from "react-router-dom";
import SignIn from "./View/Login";
import SignUp from "./View/SignUp";
import Profile from "./View/Profile";
import Search from "./View/Search";
import { withTranslation } from "react-i18next";
import Edit from "./View/Edit";
import AddProcedure from "./View/AddProcedure";
import EditProcedure from "./View/EditProcedure";
import PickProcedure from "./View/PickProcedure";
import CreateOrder from "./View/CreateOrder";
import MoreInfo from "./View/MoreInfo";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "en",
    };
  }
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route path="/login" component={SignIn} />
            <Route path="/signup" component={SignUp} />
            <Route path="/profile" component={Profile} />
            <Route path="/edit" component={Edit} />
            <Route path="/search" component={Search} />
            <Route path="/view" component={PickProcedure} />
            <Route path="/pick" component={CreateOrder} />
            <Route path="/more" component={MoreInfo} />
            <Route path="/add_procedure" component={AddProcedure} />
            <Route path="/edit_procedure" component={EditProcedure} />
            <Redirect from="/" to="/login" />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default withTranslation()(observer(App));
