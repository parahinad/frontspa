import React from 'react'
import Header from './Components/HeaderAuth'
import jwt_decode from "jwt-decode"
import ProviderProfile from './Components/ProviderProfile'
import CustomerProfile from './Components/CustomerProfile'
import AdminProfile from './Components/AdminProfile'

if(localStorage.getItem("Token") != null){
    var token = localStorage.getItem("Token")
    var decoded = jwt_decode(token)
}

class Profile extends React.Component{

    render() {

        if(localStorage.getItem("Token") == null){
            window.location.href='./'
        }else{
            if(decoded.role === "CUSTOMER"){
                return(
                    <div className="profile">
                        <Header/>
                        <CustomerProfile/>
                    </div>
                )
            } else if(decoded.role === "PROVIDER"){
                return(
                    <div className="profile">
                        <Header/>
                        <ProviderProfile/>
                    </div>
                )
            }
            else if(decoded.role === "ADMIN"){
                return(
                    <div className="profile">
                        <Header/>
                        <AdminProfile/>
                    </div>
                )
            }
        }
    }
}

export default Profile;