import React from "react";
import Header from "./Components/HeaderAuth";
import PickDateForm from "./Components/PickDateForm";
class Login extends React.Component {
  render() {
    return (
      <div className="signIn">
        <Header />
        <div className="container">
          <PickDateForm />
        </div>
      </div>
    );
  }
}

export default Login;
