import React from "react";
import Header from "./Components/HeaderAuth";
import { withTranslation } from "react-i18next";

var url = "http://localhost:8080";

class Info extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      procedure: {},
      device: {},
    };
  }

  componentDidMount() {
    fetch(`${url}/providers/procedures/${localStorage.getItem("PId")}`, {
      method: "get",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("Token"),
      },
    })
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            procedure: result,
            device: result.smartDevice,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }

  render() {
    const { t } = this.props;
    return (
      <div className="profile">
        <Header />
        <div className="profile_back">
          <p id="cName">{this.state.procedure.name}</p>
          <p></p>
          <p>
            {t("Temp")}: {this.state.device.temperature}
          </p>
          <p>
            {t("Hum")}: {this.state.device.humidity}
          </p>
        </div>
      </div>
    );
  }
}

export default withTranslation()(Info);
