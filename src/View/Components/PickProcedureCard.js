import React from "react";
import Button from "./Button";
import { withTranslation } from "react-i18next";
import jwt_decode from "jwt-decode";

if (localStorage.getItem("Token") != null) {
  var token = localStorage.getItem("Token");
  var decoded = jwt_decode(token);
}

var url = "http://localhost:8080";

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      procedures: [],
      date: "",
    };
  }

  componentDidMount() {
    var res;
    if (decoded.role === "CUSTOMER") {
      res = `${url}/providers/${localStorage.getItem(
        "providerMail"
      )}/procedures`;
    } else if (decoded.role === "PROVIDER") {
      res = `${url}/providers/${decoded.email}/procedures`;
    }
    fetch(res, {
      method: "get",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    })
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            procedures: result,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }

  render() {
    const { t } = this.props;
    const { error, isLoaded, procedures } = this.state;
    if (error) {
      return (
        <div className="additional">
          {t("Failiture")}: {error.message}
        </div>
      );
    } else if (!isLoaded) {
      return <div className="additional">{t("Loading")}...</div>;
    } else {
      return <div className="grid">{procedures.map(this.renderCard)}</div>;
    }
  }

  deleteProcedure(id) {
    fetch(`${url}/providers/procedures/${id}`, {
      method: "delete",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("Token"),
      },
    }).then(
      (result) => {
        window.location.reload();
      },
      (error) => {
        this.setState({
          isLoaded: true,
          error,
        });
      }
    );
  }

  renderCard = (procedure) => {
    const { t } = this.props;

    if (decoded.role === "CUSTOMER") {
      return (
        <div className="card text-center">
          <div className="crd-body text-dark" id={procedure.id}>
            <h2 className="card-title">{procedure.name}</h2>
            <p className="card-text text-secondary">{procedure.description}</p>
            <p className="card-text text-secondary">
              {t("Min")}: {procedure.minAge}
            </p>
            <p className="card-text text-secondary">
              {t("Max")}: {procedure.maxAge}
            </p>
            <p className="card-text text-secondary">
              {t("Price")}: {procedure.price} {t("pMes")}
            </p>
            <p className="card-text text-secondary">
              {t("Dur")}: {procedure.duration} {t("DurA")}
            </p>

            <Button
              text={t("Pick")}
              onClick={() => {
                localStorage.setItem("procId", procedure.id);
                window.location.href = "./pick";
              }}
            />
          </div>
        </div>
      );
    } else if (decoded.role === "PROVIDER") {
      return (
        <div className="card text-center">
          <div className="crd-body text-dark" id={procedure.id}>
            <h2 className="card-title">{procedure.name}</h2>
            <p className="card-text text-secondary">{procedure.description}</p>
            <p className="card-text text-secondary">
              {t("Min")}: {procedure.minAge}
            </p>
            <p className="card-text text-secondary">
              {t("Max")}: {procedure.maxAge}
            </p>
            <p className="card-text text-secondary">
              {t("Price")}: {procedure.price} {t("pMes")}
            </p>
            <p className="card-text text-secondary">
              {t("Dur")}: {procedure.duration} {t("DurA")}
            </p>
            <Button
              text={t("More")}
              onClick={() => {
                localStorage.setItem("PId", procedure.id);
                window.location.href = "./more";
              }}
            />
            <Button
              text={t("Edit")}
              onClick={(e) => {
                localStorage.setItem("PId", procedure.id);
                window.location.href = "./edit_procedure";
              }}
            />
            <Button
              text={t("Delete")}
              onClick={() => this.deleteProcedure(procedure.id)}
            />
          </div>
        </div>
      );
    }
  };
}

export default withTranslation()(Card);
