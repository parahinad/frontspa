import React from "react";
import Button from "./Button";
import { withTranslation } from "react-i18next";

import jwt_decode from "jwt-decode";
import ServiceCard from "./ProcedureCard";

if (localStorage.getItem("Token") != null) {
  var token = localStorage.getItem("Token");
  var decoded = jwt_decode(token);
}

var url = "http://localhost:8080";

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      customer: {},
      address: {},
    };
  }

  componentDidMount() {
    fetch(`${url}/customers/${decoded.email}`, {
      method: "get",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("Token"),
      },
    })
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            customer: result,
            address: result.address,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }

  render() {
    localStorage.setItem("cId", this.state.customer.id);
    const { t } = this.props;
    if (localStorage.getItem("Token") == null) {
      window.location.href = "./";
    } else {
      return (
        <div className="profile">
          <div className="profile_back">
            <p id="cName">
              {this.state.customer.firstName} {this.state.customer.lastName}
            </p>
            <p></p>
            <p id="Age">
              {t("Age")}: {this.state.customer.age}
            </p>
            <Button
              text={t("EditP")}
              disabled={false}
              onClick={() => {
                window.location.href = "./edit";
              }}
            />
            <p>
              {t("Gender")}: {this.state.customer.gender}
            </p>
            <p></p>
            <p>
              {t("Email")}: {this.state.customer.email}
            </p>
            <p></p>
            <p>
              {t("Phone")}: {this.state.customer.phoneNumber}
            </p>
            <p></p>

            <p>
              {t("Address")}: {this.state.address.country}, {t("City")}
              {this.state.address.city}, {t("Street")}
              {this.state.address.street}, {t("House")}
              {this.state.address.houseNumber}
            </p>
          </div>
          <div className="rooms_back">
            <p>{t("Services")}</p>
          </div>
          <div id="rooms_container">
            <ServiceCard />
          </div>
        </div>
      );
    }
  }
}

export default withTranslation()(Profile);
