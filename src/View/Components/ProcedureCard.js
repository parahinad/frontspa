import React from "react";
import { withTranslation } from "react-i18next";
import jwt_decode from "jwt-decode";
import Moment from "moment";
import localization from "moment/locale/uk";

if (localStorage.getItem("Token") != null) {
  var token = localStorage.getItem("Token");
  var decoded = jwt_decode(token);
}

var url = "http://localhost:8080";

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      services: [],
      date: "",
    };
  }

  componentDidMount() {
    fetch(`${url}/schedules/customer/${decoded.email}`, {
      method: "get",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    })
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            services: result,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }

  deleteService(id) {
    fetch(`${url}/cleaning-companies/cleaning-services/${id}`, {
      method: "delete",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    }).then(
      (result) => {
        window.location.reload();
      },
      (error) => {
        this.setState({
          isLoaded: true,
          error,
        });
      }
    );
  }

  render() {
    const { t } = this.props;
    const { error, isLoaded, services } = this.state;
    if (error) {
      return (
        <div className="additional">
          {t("Failiture")}: {error.message}
        </div>
      );
    } else if (!isLoaded) {
      return <div className="additional">{t("Loading")}...</div>;
    } else {
      return <div className="grid">{services.map(this.renderCard)}</div>;
    }
  }

  localTime(date) {
    if (localStorage.getItem("i18nextLng") === "EN") {
      this.state.date = Moment(date).locale("en").format("LLL");
    } else if (localStorage.getItem("i18nextLng") === "UA") {
      this.state.date = Moment(date).locale("uk", localization).format("LLL");
    }
  }

  renderCard = (service) => {
    const { t } = this.props;
    this.localTime(service.date);
    return (
      <div className="card text-center">
        <div className="crd-body text-dark" id={service.id}>
          <h2 className="card-title">{service.procedureName}</h2>
          <p className="card-text text-secondary">
            {t("Price")}: {service.price} {t("pMes")}
          </p>
          <p className="card-text text-secondary">
            {t("DName")}: {service.providerName}
          </p>
          <p className="card-text text-secondary">
            {t("Dur")}: {service.duration} {t("DurA")}
          </p>
          <p className="card-text text-secondary">
            {t("Date")}: {this.state.date}
          </p>
        </div>
      </div>
    );
  };
}

export default withTranslation()(Card);
