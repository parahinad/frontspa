import React from "react";
import Input from "./Input";
import Button from "./Button";
import { withTranslation } from "react-i18next";
import jwt_decode from "jwt-decode";

var url = "http://localhost:8080";
if (localStorage.getItem("Token") != null) {
  var token = localStorage.getItem("Token");
  var decoded = jwt_decode(token);
}

class EditForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      lname: "",
      gender: "",
      age: "",
      address: {},
      email: "",
      phone: "",
      id: "",
      password: "",
      flag: 1,
      buttonDisabled: false,
    };
  }

  setInputValue(property, val) {
    val = val.trim();
    this.setState({
      [property]: val,
    });
  }

  resetForm() {
    this.setState({
      name: "",
      email: "",
      phone: "",
      city: "",
      name: "",
      lname: "",
      gender: "",
      age: "",
      country: "",
      password: "",
      street: "",
      house: "",
      buttonDisabled: false,
    });
  }

  componentDidMount() {
    if (decoded.role === "CUSTOMER") {
      this.getData(`${url}/customers/${decoded.email}`);
    } else if (decoded.role === "ADMIN") {
      if (localStorage.getItem("Role") === "CUSTOMER") {
        this.getData(`${url}/customers/${localStorage.getItem("Email")}`);
      }
    }
  }

  getData(resUrl) {
    fetch(resUrl, {
      method: "get",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    })
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            name: result.firstName,
            lname: result.lastName,
            age: result.age,
            gender: result.gender,
            email: result.email,
            phone: result.phoneNumber,
            address: result.address,
            id: result.id,
            company: result,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }

  checkEmail(email) {
    let regEmail = new RegExp(
      "^([a-z0-9_-]+.)*[a-z0-9_-]+@[a-z0-9_-]+(.[a-z0-9_-]+)*.[a-z]{2,6}$"
    );
    if (!regEmail.test(email)) {
      this.setState({ flag: 2 });
      return false;
    }
    return true;
  }
  checkPass(password) {
    if (password.length < 8) {
      this.setState({ flag: 3 });
      return false;
    }
    return true;
  }

  checkName(name) {
    let regName = new RegExp("^([А-ЯЁа-яё0-9]+)|([A-Za-z0-9]+)$");
    if (!regName.test(name)) {
      this.setState({ flag: 4 });
      return false;
    }
    return true;
  }
  checkLName(lname) {
    let regName = new RegExp("^([А-ЯЁа-яё0-9]+)|([A-Za-z0-9]+)$");
    if (!regName.test(lname)) {
      this.setState({ flag: 11 });
      return false;
    }
    return true;
  }
  checkGender(gender) {
    let regGender = new RegExp("^([А-ЯЁа-яё0-9]+)|([A-Za-z0-9]+)$");
    if (!regGender.test(gender)) {
      this.setState({ flag: 12 });
      return false;
    }
    return true;
  }
  checkAge(age) {
    let regAge = new RegExp("^[0-9]+$");
    if (!regAge.test(age)) {
      this.setState({ flag: 13 });
      return false;
    }
    return true;
  }
  checkPhone(phone) {
    let regPhone = new RegExp("^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-s./0-9]*$");
    if (!regPhone.test(phone)) {
      this.setState({ flag: 5 });
      return false;
    }
    return true;
  }

  checkCountry(country) {
    let regCountry = new RegExp("^([а-яё]+)|([a-z]+)$");
    if (!regCountry.test(country)) {
      this.setState({ flag: 6 });
      return false;
    }
    return true;
  }

  checkCity(city) {
    let regCity = new RegExp("^([а-яё]+)|([a-z]+)$");
    if (!regCity.test(city)) {
      this.setState({ flag: 7 });
      return false;
    }
    return true;
  }

  checkStreet(street) {
    let regStreet = new RegExp("^([а-яё]+)|([a-z]+)$");
    if (!regStreet.test(street)) {
      this.setState({ flag: 8 });
      return false;
    }
    return true;
  }

  checkHouseNum(house) {
    let regHouse = new RegExp("^([0-9A-Za-zА-яа-яёЁ]+)$");
    if (!regHouse.test(house)) {
      this.setState({ flag: 9 });
      return false;
    }
    return true;
  }

  checkCred() {
    if (!this.checkName(this.state.name)) {
      return;
    }
    if (!this.checkLName(this.state.lname)) {
      return;
    }
    if (!this.checkAge(this.state.age)) {
      return;
    }
    if (!this.checkGender(this.state.gender)) {
      return;
    }
    if (!this.checkEmail(this.state.email)) {
      return;
    }
    if (!this.checkPass(this.state.password)) {
      return;
    }
    if (!this.checkPhone(this.state.phone)) {
      return;
    }
    if (!this.checkCountry(this.state.country)) {
      return;
    }
    if (!this.checkCity(this.state.city)) {
      return;
    }
    if (!this.checkStreet(this.state.street)) {
      return;
    }
    if (!this.checkHouseNum(this.state.house)) {
      return;
    }

    this.setState({
      buttonDisabled: true,
    });

    // if (decoded.role === "CUSTOMER") {
    //   this.editCompany(`${url}/customers`);
    // } else if (decoded.role === "PROVIDER") {
    //   this.editCompany(`${url}/providers`);
    // } else if (decoded.role === "ADMIN") {
    //   if (localStorage.getItem("Role") === "CUSTOMER") {
    //     this.editCompany(`${url}/customer-companies`);
    //   } else if (localStorage.getItem("Role") === "PROVIDER") {
    //     this.editCompany(`${url}/providers`);
    //   }
    // }

    this.editCompany(`${url}/customers`);
  }

  async editCompany(resUrl) {
    try {
      var role;
      if (decoded.role !== "ADMIN") {
        role = decoded.role;
      } else {
        role = localStorage.getItem("Role");
      }
      let res = await fetch(resUrl, {
        method: "put",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("Token"),
        },
        body: JSON.stringify({
          address: {
            city: this.state.address.city,
            country: this.state.address.country,
            houseNumber: this.state.address.houseNumber,
            latitude: this.state.address.latitude,
            longitude: this.state.address.longitude,
            street: this.state.address.street,
          },
          firstName: this.state.name,
          lastName: this.state.lname,
          age: this.state.age,
          gender: this.state.gender,
          email: this.state.email,
          id: this.state.id,
          phoneNumber: this.state.phone,
          password: this.state.password,
          role: role,
        }),
      });
      let result = await res.json();
      if (result && result.id !== null) {
        window.location.href = "./profile";
      } else if (result) {
        this.resetForm();
        this.setState({ flag: 10 });
      }
    } catch (e) {
      console.log(e);
      this.resetForm();
    }
  }

  render() {
    const { t } = this.props;
    return (
      <div className="signUpForm">
        <div className="signUpContainer">
          <h1>{t("Edit")}</h1>
          {this.state.flag === 2 && <p>{t("EEmail")}</p>}
          {this.state.flag === 3 && <p>{t("EPass")}</p>}
          {this.state.flag === 4 && <p>{t("EFName")}</p>}
          {this.state.flag === 5 && <p>{t("EPhone")}</p>}
          {this.state.flag === 6 && <p>{t("ECountry")}</p>}
          {this.state.flag === 7 && <p>{t("ECity")}</p>}
          {this.state.flag === 8 && <p>{t("EStreet")}</p>}
          {this.state.flag === 9 && <p>{t("EHouse")}</p>}
          {this.state.flag === 10 && <p>{t("EError")}</p>}
          {this.state.flag === 11 && <p>{t("ELName")}</p>}
          {this.state.flag === 12 && <p>{t("EGender")}</p>}
          {this.state.flag === 13 && <p>{t("EAge")}</p>}
          <Input
            type="text"
            placeholder={t("DFName")}
            value={this.state.name ? this.state.name : ""}
            onChange={(val) => this.setInputValue("name", val)}
          />
          <Input
            type="text"
            placeholder={t("DLName")}
            value={this.state.lname ? this.state.lname : ""}
            onChange={(val) => this.setInputValue("lname", val)}
          />
          <Input
            type="text"
            placeholder={t("Age")}
            value={this.state.age ? this.state.age : ""}
            onChange={(val) => this.setInputValue("age", val)}
          />
          <Input
            type="text"
            placeholder={t("Gender")}
            value={this.state.gender ? this.state.gender : ""}
            onChange={(val) => this.setInputValue("gender", val)}
          />
          <Input
            type="text"
            placeholder={t("Email")}
            value={this.state.email ? this.state.email : ""}
            onChange={(val) => this.setInputValue("email", val)}
          />
          <Input
            type="password"
            placeholder={t("Password")}
            value={this.state.password ? this.state.password : ""}
            onChange={(val) => this.setInputValue("password", val)}
          />
          <Input
            type="text"
            placeholder={t("Phone")}
            value={this.state.phone ? this.state.phone : ""}
            onChange={(val) => this.setInputValue("phone", val)}
          />
          <Input
            type="text"
            placeholder={t("FCountry")}
            value={this.state.address.country ? this.state.address.country : ""}
            onChange={(val) => this.setInputValue("country", val)}
          />
          <Input
            type="text"
            placeholder={t("FCity")}
            value={this.state.address.city ? this.state.address.city : ""}
            onChange={(val) => this.setInputValue("city", val)}
          />
          <Input
            type="text"
            placeholder={t("FStreet")}
            value={this.state.address.street ? this.state.address.street : ""}
            onChange={(val) => this.setInputValue("street", val)}
          />
          <Input
            type="text"
            placeholder={t("FHouse")}
            value={
              this.state.address.houseNumber
                ? this.state.address.houseNumber
                : ""
            }
            onChange={(val) => this.setInputValue("house", val)}
          />
          <Button
            text={t("Save")}
            disabled={this.state.buttonDisabled}
            onClick={() => this.checkCred()}
          />
        </div>
      </div>
    );
  }
}

export default withTranslation()(EditForm);
