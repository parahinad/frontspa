import React from "react";
import Input from "./Input";
import Button from "./Button";
import { withTranslation } from "react-i18next";
import jwt_decode from "jwt-decode";

var url = "http://localhost:8080";
if (localStorage.getItem("Token") != null) {
  var token = localStorage.getItem("Token");
  var decoded = jwt_decode(token);
}

class AddSForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      duration: "",
      desc: "",
      maxAr: 0,
      minAr: 0,
      name: "",
      price: 0,
      buttonDisabled: false,
    };
  }

  setInputValue(property, val) {
    val = val.trim();
    this.setState({
      [property]: val,
    });
  }

  resetForm() {
    this.setState({
      duration: "",
      desc: "",
      maxAr: 0,
      minAr: 0,
      name: "",
      price: 0,
      buttonDisabled: false,
    });
  }

  checkName(name) {
    let editName = new RegExp("^([А-ЯЁа-яё0-9]+)|([A-Za-z0-9]+)$");
    if (!editName.test(name)) {
      this.setState({ flag: 2 });
      return false;
    }
    return true;
  }

  checkminA(minA) {
    let editMinA = new RegExp("^([0-9]+)$");
    if (!editMinA.test(minA)) {
      this.setState({ flag: 3 });
      return false;
    }
    return true;
  }
  checkmaxA(maxA) {
    let editMaxA = new RegExp("^([0-9]+)$");
    if (!editMaxA.test(maxA)) {
      this.setState({ flag: 4 });
      return false;
    }
    return true;
  }

  checkDesc(desc) {
    if (desc.length < 1) {
      this.setState({ flag: 5 });
      return false;
    }
    return true;
  }

  checkdur(dur) {
    let editType = new RegExp("^([0-9]+)$");
    if (!editType.test(dur)) {
      this.setState({ flag: 6 });
      return false;
    }
    return true;
  }

  checkPrice(price) {
    let editPrice = new RegExp("^([0-9.]+)$");
    if (!editPrice.test(price)) {
      this.setState({ flag: 7 });
      return false;
    }
    return true;
  }

  checkCred() {
    if (!this.checkName(this.state.name)) {
      return;
    }
    if (!this.checkDesc(this.state.desc)) {
      return;
    }
    if (!this.checkminA(this.state.minA)) {
      return;
    }
    if (!this.checkmaxA(this.state.maxA)) {
      return;
    }
    if (!this.checkdur(this.state.duration)) {
      return;
    }
    if (!this.checkprice(this.state.price)) {
      return;
    }
    this.setState({
      buttonDisabled: true,
    });

    this.addService();
  }

  async addService() {
    try {
      let res = await fetch(`${url}/providers/${decoded.email}/procedures`, {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("Token"),
        },
        body: JSON.stringify({
          description: this.state.desc,
          maxAge: this.state.maxAr,
          minAge: this.state.minAr,
          name: this.state.name,
          price: this.state.price,
          duration: this.state.duration,
        }),
      });
      let result = await res.json();
      if (result) {
        window.location.href = "./profile";
      }
    } catch (e) {
      console.log(e);
      this.resetForm();
    }
  }

  render() {
    const { t } = this.props;
    return (
      <div className="signUpForm">
        <div className="signUpContainer">
          <h1>{t("AddS")}</h1>
          {this.state.flag === 2 && <p>{t("EName")}</p>}
          {this.state.flag === 3 && <p>{t("EMinA")}</p>}
          {this.state.flag === 4 && <p>{t("EMaxA")}</p>}
          {this.state.flag === 5 && <p>{t("EDesc")}</p>}
          {this.state.flag === 6 && <p>{t("EType")}</p>}
          {this.state.flag === 7 && <p>{t("Eprice")}</p>}
          {this.state.flag === 10 && <p>{t("EError")}</p>}
          <Input
            type="text"
            placeholder={t("PName")}
            value={this.state.name ? this.state.name : ""}
            onChange={(val) => this.setInputValue("name", val)}
          />
          <Input
            type="text"
            placeholder={t("Dur")}
            value={this.state.duration ? this.state.duration : ""}
            onChange={(val) => this.setInputValue("duration", val)}
          />

          <Input
            type="text"
            placeholder={t("Desc")}
            value={this.state.desc ? this.state.desc : ""}
            onChange={(val) => this.setInputValue("desc", val)}
          />
          <Input
            type="text"
            placeholder={t("Min")}
            value={this.state.minAr ? this.state.minAr : ""}
            onChange={(val) => this.setInputValue("minAr", val)}
          />
          <Input
            type="text"
            placeholder={t("Max")}
            value={this.state.maxAr ? this.state.maxAr : ""}
            onChange={(val) => this.setInputValue("maxAr", val)}
          />
          <Input
            type="text"
            placeholder={t("Price")}
            value={this.state.price ? this.state.price : ""}
            onChange={(val) => this.setInputValue("price", val)}
          />
          <Button
            text={t("Add")}
            disabled={this.state.buttonDisabled}
            onClick={() => this.checkCred()}
          />
        </div>
      </div>
    );
  }
}

export default withTranslation()(AddSForm);
