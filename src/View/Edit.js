import React from "react";
import Header from "./Components/HeaderAuth";
import EditForm from "./Components/EditForm";
import EditCustomerForm from "./Components/EditCustomerForm";
import jwt_decode from "jwt-decode";

if (localStorage.getItem("Token") != null) {
  var token = localStorage.getItem("Token");
  var decoded = jwt_decode(token);
}

class Edit extends React.Component {
  render() {
    if (
      decoded.role === "CUSTOMER" ||
      localStorage.getItem("Role") === "CUSTOMER"
    ) {
      return (
        <div className="signIn">
          <Header />
          <div className="container">
            <EditCustomerForm />
          </div>
        </div>
      );
    } else if (
      decoded.role === "PROVIDER" ||
      localStorage.getItem("Role") === "PROVIDER"
    ) {
      return (
        <div className="signIn">
          <Header />
          <div className="container">
            <EditForm />
          </div>
        </div>
      );
    }
  }
}

export default Edit;
