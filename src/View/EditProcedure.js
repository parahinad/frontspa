import React from "react";
import Header from "./Components/HeaderAuth";
import EditService from "./Components/EditProcedureForm";
class Edit extends React.Component {
  render() {
    return (
      <div className="signIn">
        <Header />
        <div className="container">
          <EditService />
        </div>
      </div>
    );
  }
}

export default Edit;
